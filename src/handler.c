/* handler.c: HTTP Request Handlers */

#include "spidey.h"

#include <errno.h>
#include <limits.h>
#include <string.h>

#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>

/* Internal Declarations */
Status handle_browse_request(Request *request);
Status handle_file_request(Request *request);
Status handle_cgi_request(Request *request);
Status handle_error(Request *request, Status status);

/**
 * Handle HTTP Request.
 *
 * @param   r           HTTP Request structure
 * @return  Status of the HTTP request.
 *
 * This parses a request, determines the request path, determines the request
 * type, and then dispatches to the appropriate handler type.
 *
 * On error, handle_error should be used with an appropriate HTTP status code.
 **/
Status  handle_request(Request *r) {
    Status result;

    /* Parse request */
    if(parse_request(r) == -1){
        result = HTTP_STATUS_BAD_REQUEST;
        goto done;
    }

    /* Determine request path */
    r->path = determine_request_path(r->uri);
    if(!(r->path)){
        result = HTTP_STATUS_NOT_FOUND;
        goto done;
    }
    debug("HTTP REQUEST PATH: %s", r->path);

    /* Dispatch to appropriate request handler type based on file type */
    struct stat info;
    stat(r->path, &info);
    if(S_ISDIR(info.st_mode)){
        result = handle_browse_request(r);
    }
    else if (!(access(r->path, X_OK)))
    {
        result = handle_cgi_request(r);
        goto done;
    }
    else if (!(access(r->path, R_OK)))
    {
        result = handle_file_request(r);
        goto done;
    }
    else{
        result = HTTP_STATUS_BAD_REQUEST;
        goto done;
    }
    



done:
    log("HTTP REQUEST STATUS: %s", http_status_string(result));
    return handle_error(r, result);
}

/**
 * Handle browse request.
 *
 * @param   r           HTTP Request structure.
 * @return  Status of the HTTP browse request.
 *
 * This lists the contents of a directory in HTML.
 *
 * If the path cannot be opened or scanned as a directory, then handle error
 * with HTTP_STATUS_NOT_FOUND.
 **/
Status  handle_browse_request(Request *r) {
    struct dirent **entries;
    int n;

    /* Open a directory for reading or scanning */
    if((n = scandir(r->path, &entries, NULL, alphasort) ) < 0){
        debug("scandirL %s", strerror(errno));
        return HTTP_STATUS_NOT_FOUND;
    }

    /* Write HTTP Header with OK Status and text/html Content-Type */
    fprintf(r->stream, "HTTP/1.0 200 OK\n");
    fprintf(r->stream, "Content-Type: text/html\n");
    fprintf(r->stream, "\r\n");
    fprintf(r->stream, "<html>\n");
    fprintf(r->stream,"<head>\n\t<title>Spidey</title>\n</head>\n");
    fprintf(r->stream,"<body>\n");
    fprintf(r->stream, "\t<ul>\n");
    
    char *temp = strdup(r->uri);
    if(temp[strlen(temp) -1] == '/'){
        temp[strlen(temp) - 1] = '\0';
    }

    /* For each entry in directory, emit HTML list item */
    for(int i = 1; i < n; i++){
        fprintf(r->stream,"\t\t<li><a href=\"%s/%s\">%s</a></li>\n", temp, entries[i]->d_name, entries[i]->d_name);
        free(entries[i]);
    }

    fprintf(r->stream, "\t</ul>\n</body>\n</html>\n\r\n");
    /* Return OK */
    free(temp);
    free(entries[0]);
    free(entries);
    fflush(r->stream);
     
    return HTTP_STATUS_OK;
}

/**
 * Handle file request.
 *
 * @param   r           HTTP Request structure.
 * @return  Status of the HTTP file request.
 *
 * This opens and streams the contents of the specified file to the socket.
 *
 * If the path cannot be opened for reading, then handle error with
 * HTTP_STATUS_NOT_FOUND.
 **/
Status  handle_file_request(Request *r) {
    FILE *fs;
    char buffer[BUFSIZ];
    char *mimetype = NULL;
    size_t nread;
    size_t nwrite;


    /* Open file for reading */
    fs = fopen(r->path,"r");
    if(!fs){
        debug("failed to open file path: %s", r->path);
        debug("specified error: %s", strerror(errno));
        goto fail;
    }

    /* Determine mimetype */
    mimetype = determine_mimetype(r->path);

    /* Write HTTP Headers with OK status and determined Content-Type */
    fprintf(r->stream,"HTTP/1.0 200 OK\nContent-Type: %s\n\r\n", mimetype);

    /* Read from file and write to socket in chunks */
    nread = fread(buffer, sizeof(char), BUFSIZ, fs);

    /* Keep reading from file until hit NULL*/
    while(nread){
        nwrite = 0;
        nwrite = fwrite(buffer, sizeof(char), nread,r->stream);

        while(nwrite <nread){
            nwrite += fwrite(buffer + nwrite, sizeof(char), nread, r->stream);
        }

        nread = fread(buffer, sizeof(char), BUFSIZ, fs);
    }

    fclose(fs);
    fflush(r->stream);
    free(mimetype);

    /* Close file, deallocate mimetype, return OK */
    
    return HTTP_STATUS_OK;

fail:
    /* Close file, free mimetype, return INTERNAL_SERVER_ERROR */
    return HTTP_STATUS_INTERNAL_SERVER_ERROR;
}

/**
 * Handle CGI request
 *
 * @param   r           HTTP Request structure.
 * @return  Status of the HTTP file request.
 *
 * This popens and streams the results of the specified executables to the
 * socket.
 *
 * If the path cannot be popened, then handle error with
 * HTTP_STATUS_INTERNAL_SERVER_ERROR.
 **/
Status  handle_cgi_request(Request *r) {
    FILE *pfs;
    char buffer[BUFSIZ];
    Header *header;

    /* Export CGI environment variables from request:
     * http://en.wikipedia.org/wiki/Common_Gateway_Interface */

    if (setenv("REQUEST_METHOD", r->method, 1)){
        debug("setenv: %s", strerror(errno));
    }
    if (setenv("REQUEST_URI", r->uri, 1)){
        debug("setenv: %s", strerror(errno));
    }
    if (setenv("SCRIPT_FILENAME", r->path, 1)){
        debug("setenv: %s", strerror(errno));
    }
    if (setenv("QUERY_STRING", r->query, 1)){
        debug("setenv: %s", strerror(errno));
    }
    if (setenv("REMOTE_ADDR", r->host, 1)){
        debug("setenv: %s", strerror(errno));
    }
    if (setenv("REMOTE_PORT", r->port, 1)){
        debug("setenv: %s", strerror(errno));
    }
    
    
    
    if (setenv("SERVER_PORT", Port, 1)){
        debug("setenv: %s", strerror(errno));
    }
    if (setenv("DOCUMENT_ROOT", RootPath, 1)){
         debug("setenv: %s", strerror(errno));
    }
    /* Export CGI environment variables from request headers */

    header = r->headers;
    while(header){
        /*I don't know if this shit is right, just copy and pasted every single
        header variable that cgi requests take with the description containing 
        the word header and included it*/
        if(streq(header->name, "Accept-Charset")){
            if(setenv("HTTP_ACCEPT_CHARSET", header->data,1)){
                debug("setenv: %s", strerror(errno));
            }
        }
        else if(streq(header->name, "Accept-Encoding")){
            if(setenv("HTTP_ACCEPT_ENCODING", header->data,1)){
                debug("setenv: %s", strerror(errno));
            }
        }
        else if(streq(header->name, "Accept-Language")){
            if(setenv("HTTP_ACCEPT_LANGUAGE", header->data,1)){
                debug("setenv: %s", strerror(errno));
            }
        }
        else if(streq(header->name, "Connection")){
            if(setenv("HTTP_CONNECTION", header->data,1)){
                debug("setenv: %s", strerror(errno));
            }
        }
        else if(streq(header->name, "Host")){
            if(setenv("HTTP_HOST", header->data,1)){
                debug("setenv: %s", strerror(errno));
            }
        }
        else if(streq(header->name, "User-Agent")){
            if(setenv("HTTP_USER_AGENT", header->data,1)){
                debug("setenv: %s", strerror(errno));
            }
        }
        header = header->next;
    }

    /* POpen CGI Script */
    pfs = popen(r->path, "r");
    if(!pfs){
        debug("popen: %s", strerror(errno));
        return HTTP_STATUS_INTERNAL_SERVER_ERROR;
    }

    /* Copy data from popen to socket */
    while(fgets(buffer, BUFSIZ, pfs)){
        fputs(buffer, r->stream);
    }
    /* Close popen, return OK */
    pclose(pfs);
    fflush(r->stream);
    return HTTP_STATUS_OK;
}

/**
 * Handle displaying error page
 *
 * @param   r           HTTP Request structure.
 * @return  Status of the HTTP error request.
 *
 * This writes an HTTP status error code and then generates an HTML message to
 * notify the user of the error.
 **/
Status  handle_error(Request *r, Status status) {
    

    /* Write HTTP Header */
    if(status != HTTP_STATUS_OK){
        const char *status_string = http_status_string(status);
        fprintf(r->stream, "HTTP/1.0 %s\n", status_string);
        fprintf(r->stream, "Content-Type: text/html\n");
        fprintf(r->stream, "\r\n");
        fprintf(r->stream, "<html>\n<head>%s</head>\n<body>\t%s</body></html>", status_string, status_string);
        fflush(r->stream);
    }


    /* Write HTML Description of Error*/

    /* Return specified status */
    return status;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
