/* forking.c: Forking HTTP Server */

#include "spidey.h"

#include <errno.h>
#include <signal.h>
#include <string.h>

#include <unistd.h>

/**
 * Fork incoming HTTP requests to handle the concurrently.
 *
 * @param   sfd         Server socket file descriptor.
 * @return  Exit status of server (EXIT_SUCCESS).
 *
 * The parent should accept a request and then fork off and let the child
 * handle the request.
 **/
int forking_server(int sfd) {
    pid_t pid;
    Request *request;
    /* Accept and handle HTTP request */
    while (true) {
    	/* Accept request */
        request = accept_request(sfd);
	/* Ignore children */
        signal(SIGCHLD,SIG_IGN);
	/* Fork off child process to handle request */
        pid = fork();
        if(pid == -1){
            debug("fork error: %s", strerror(errno));
            free_request(request);
        }
        else if(pid == 0){
            close(sfd);
            handle_request(request);
            exit(EXIT_SUCCESS);
        }
        else{
            free_request(request);
        }
    }
    close(sfd);

    /* Close server socket */
    return EXIT_SUCCESS;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
