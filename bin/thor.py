#!/usr/bin/env python3

import concurrent.futures
import os
import requests
import sys
import time

# Functions

def usage(status=0):
    progname = os.path.basename(sys.argv[0])
    print(f'''Usage: {progname} [-h HAMMERS -t THROWS] URL
    -h  HAMMERS     Number of hammers to utilize (1)
    -t  THROWS      Number of throws per hammer  (1)
    -v              Display verbose output
    ''')
    sys.exit(status)

def hammer(url, throws, verbose, hid):
    ''' Hammer specified url by making multiple throws (ie. HTTP requests).

    - url:      URL to request
    - throws:   How many times to make the request
    - verbose:  Whether or not to display the text of the response
    - hid:      Unique hammer identifier

    Return the average elapsed time of all the throws.
    '''

    times = []


    for x in range(throws):
        start = time.time()
        response = requests.get(url)
        end = time.time()

        duration = end - start
        times.append(duration)

        print(f"Hammer:{hid:2}, Throw:{x:4}, Elapsed Time: {duration:4}")

        if verbose:
            print(response.text)
    
    average = sum(times)/len(times)

    print(f'Hammer:{hid:2}, AVERAGE   , Elapsed Time:{average:4}')

    return average

def do_hammer(args):
    ''' Use args tuple to call `hammer` '''
    return hammer(*args)

def main():
    hammers = 1
    throws  = 1
    verbose = False
    url = None


    # Parse command line arguments
    if len(sys.argv) == 1:
        return usage(1)

    args = sys.argv[1:]
    
    while len(args) > 0:
        if args[0] == '-h':
            args.pop(0)
            hammers = int(args[0])
        if args[0] == '-t':
            args.pop(0)
            throws = int(args[0])
        if args[0] == '-v':
            verbose = True
        if args[0].startswith('http'):
            url = args[0]

        args.pop(0)
    

    if not url:
        return usage(1)

    # Create pool of workers and perform throws
    with concurrent.futures.ProcessPoolExecutor(max_workers=hammers) as executor:
        

        arguments = ((url, throws, verbose, hid) for hid in range(hammers))

        final = list(executor.map(do_hammer,arguments))        
        total_avg = sum(final)/float(hammers)
        print(f'TOTAL AVERAGE ELAPSED TIME: {total_avg}')
        return 0


# Main execution

if __name__ == '__main__':
    main()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
